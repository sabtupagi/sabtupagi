package com.example.android.weatherforecast;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;

import com.example.BaseApi;

/**
 * Created by pratama on 7/16/2016.
 */
public class BaseApp extends Application {
    public static BaseApi getApi() {
        return new BaseApi();
    }

}
