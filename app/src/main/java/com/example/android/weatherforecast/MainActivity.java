package com.example.android.weatherforecast;

import android.os.Bundle;
import android.util.Log;

import com.example.MyObserver;
import com.example.dao.ForecastApiDao;
import com.example.dao.WeatherApiDao;
import com.example.repository.ForecastRepository;
import com.example.repository.WeatherRepository;

import rx.schedulers.Schedulers;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getForecast();
    }


    private void getWeather() {
        WeatherRepository repo = new WeatherRepository(BaseApp.getApi(), "jakarta", this.getString(R.string.appid));
        addSubscription(repo.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MyObserver<WeatherApiDao>() {
                    @Override
                    public void onApiResultCompleted() {

                    }

                    @Override
                    public void onApiResultOk(WeatherApiDao weatherApiDao) {
                        Log.wtf("test_", weatherApiDao.name+"");
                    }

                    @Override
                    public void onApiResultError(String message) {
                        Log.wtf("test_", "fail:-" + message);
                    }

                })
        );
    }

    private void getForecast() {
        ForecastRepository repo = new ForecastRepository(BaseApp.getApi(), "1642911", this.getString(R.string.appid));
        addSubscription(repo.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new MyObserver<ForecastApiDao>() {
                    @Override
                    public void onApiResultCompleted() {

                    }

                    @Override
                    public void onApiResultOk(ForecastApiDao forecastApiDao) {
                        for (int i = 0; i < forecastApiDao.list.size(); i++) {
                            Log.wtf("test_",  "tgl : "+forecastApiDao.list.get(i).dt_txt+" cuacanya : "+forecastApiDao.list.get(i).weather.get(0).description);
                        }

                    }

                    @Override
                    public void onApiResultError(String message) {
                        Log.wtf("test_", "fail:-" + message);
                    }

                })
        );
    }
}
