package com.example.repository;

import com.example.BaseApi;
import com.example.dao.ForecastApiDao;
import com.example.dao.WeatherApiDao;

import rx.Observable;


public class ForecastRepository {
    BaseApi mBaseApi;
    private String id;
    private String appid;

    public ForecastRepository(BaseApi mBaseApi, String id, String appid) {
        this.mBaseApi = mBaseApi;
        this.id = id;
        this.appid = appid;
    }

    public Observable<ForecastApiDao> getData() {
        return mBaseApi.getService().getForecast(id, appid);
    }
}
