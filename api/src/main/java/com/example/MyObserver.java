package com.example;


import com.example.dao.BaseApiDao;

import rx.Observer;

public abstract class MyObserver<T extends BaseApiDao> implements Observer<T> {

    @Override
    public final void onCompleted() {
        onApiResultCompleted();
    }

    @Override
    public final void onError(Throwable e) {
        onApiResultError(e.getMessage());
        onApiResultCompleted();
    }

    @Override
    public final void onNext(T t) {
        if (t.getCod() == 200) {
            onApiResultOk(t);
        } else {
            onApiResultError(t.getMessage());
        }
    }

    public abstract void onApiResultCompleted();

    public abstract void onApiResultOk(T t);

    protected abstract void onApiResultError(String error);


}