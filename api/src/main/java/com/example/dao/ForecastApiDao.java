package com.example.dao;

import java.util.List;

/**
 * Created by pratama on 7/16/2016.
 */
public class ForecastApiDao extends BaseApiDao {

    public String cnt;
    public List<listData>list;
    public class listData{
        public String dt;
        public mainData main;
        public List<weatherData> weather;
        public cloudsData clouds;
        public windData wind;
        public String dt_txt;

    }
    public class mainData{
        public String temp;
        public String temp_min;
        public String temp_max;
        public String pressure;
        public String sea_level;
        public String grnd_level;
        public String humidity;
        public String temp_kf;
    }
    public class weatherData {
        public String id;
        public String main;
        public String description;
        public String icon;

    }

    public class cloudsData{
        public String all;
    }

    public class windData {
        public String speed;
        public String deg;
    }


}
