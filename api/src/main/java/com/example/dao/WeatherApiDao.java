package com.example.dao;

import java.util.List;

/**
 * Created by pratama on 7/16/2016.
 */
public class WeatherApiDao extends BaseApiDao {
    public String dt;
    public String name;
    public String id;
    public String base;
    public Coord coord;
    public List<Weather> weather;
    public Main main;
    public Wind wind;
    public Clouds clouds;
    public Sys sys;

    public class Coord {
        public String lon;
        public String lat;
    }

    public class Weather {
        public String id;
        public String main;
        public String description;
        public String icon;
    }

    public class Main {
        public String temp;
        public String pressure;
        public String humidity;
        public String temp_min;
        public String temp_max;
    }

    public class Wind {
        public String speed;
        public String deg;
    }

    public class Clouds {
        public String all;
    }

    public class Sys {
        public String type;
        public String id;
        public String message;
        public String country;
        public String sunrise;
        public String sunset;
    }

}
